/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.filter;

import java.io.IOException;

import java.util.List;
import java.util.StringTokenizer;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author onelove
 */
@Provider
public class SecurityFilter implements ContainerRequestFilter {

    private static final String AUTHORIZATION_HEADER_KEY = "Authorization";
    private static final String AUTHORIZATION_HEADER_PREFIX = "Basic ";
    private static final String SECURED_URL_PREFIX = "secured";
    private static final String SECURED_URL_PREFIX_auth = "auth";

    public SecurityFilter() {
        System.out.println("[Dev - Info]   Default constractor for Basic Auth Filter");

    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        if (requestContext.getUriInfo().getPath().contains(SECURED_URL_PREFIX)) {
            System.out.println("[Dev - Info]  Security Filter applied on url :: " + requestContext.getUriInfo().getPath().toString());
            //check the header to find out if the token value is set .
            if (requestContext.getHeaderString("token") != null) {
                System.out.println("[Dev - Info]  Token authentication aplied with token :: " + requestContext.getHeaderString("token"));
                //authenticates by token if the value token exists
                if (authenticateUserByToken(requestContext.getHeaderString("token"))) {
                    return;
                }
            }
            List<String> authHeader = requestContext.getHeaders().get(AUTHORIZATION_HEADER_KEY);
            try {
                String authTocken = authHeader.get(0);
                if (authHeader.size() > 0 && authTocken != null && authTocken.isEmpty() != true) {

                    authTocken = authTocken.replaceFirst(AUTHORIZATION_HEADER_PREFIX, "");
                    byte[] decoded = Base64.decodeBase64(authTocken);
                    StringTokenizer tockenizer = new StringTokenizer(new String(decoded), ":");
                    String username = tockenizer.nextToken();
                    String password = tockenizer.nextToken();
                    if (!username.isEmpty()) {
                        System.out.println("[Dev - Info]  Credetials in place ");
                        if (authenticateUser(username, password)) {
                            System.out.println("[Dev - Info]  Passed authentication from security Filter");
                            return;
                        }
                    }
                }
            } catch (NullPointerException ex) {
                Response unauthorizedStatus = Response.status(Response.Status.UNAUTHORIZED).entity("You must provide a valid username & password with Basic Auth method...").build();
                requestContext.abortWith(unauthorizedStatus);
            }
            Response unauthorizedStatus = Response.status(Response.Status.UNAUTHORIZED).entity("User cannot access the resource").build();
            requestContext.abortWith(unauthorizedStatus);
        }
    }

    private boolean authenticateUser(String username, String password) {
        return true;
    }

    public boolean authenticateUserByToken(String token) {
        return true;
    }

}
