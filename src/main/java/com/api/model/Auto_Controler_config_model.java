/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ghost
 */
@XmlRootElement
public class Auto_Controler_config_model implements Serializable {

    @XmlElement
    private String ip;
    @XmlElement
    private String port;
    @XmlElement
    private int device_ttl;
    @XmlElement
    private double critical_temprature_max;
    @XmlElement
    private double critical_temprature_min;
    @XmlElement
    private double critical_humidity_max;
    @XmlElement
    private double critical_humidity_min;
    @XmlElement
    private String open_lights_time;
    @XmlElement
    private String close_lights_time;
    @XmlElement
    private String action;
    @XmlElement
    private String device;

    public Auto_Controler_config_model() {
    }

    public Auto_Controler_config_model(int device_ttl,
            double critical_temprature_max,
            double critical_temprature_min,
            double critical_humidity_max,
            double critical_humidity_min) {
        this.setCritical_humidity_max(critical_humidity_max);
        this.setCritical_humidity_min(critical_humidity_min);
        this.setCritical_temprature_max(critical_temprature_max);
        this.setCritical_temprature_min(critical_temprature_min);
        this.setDevice_ttl(device_ttl);
    }

    public Auto_Controler_config_model(String start_date, String finish_date, int device_ttl,
            double critical_temprature_max,
            double critical_temprature_min,
            double critical_humidity_max,
            double critical_humidity_min) {
        this.setCritical_humidity_max(critical_humidity_max);
        this.setCritical_humidity_min(critical_humidity_min);
        this.setCritical_temprature_max(critical_temprature_max);
        this.setCritical_temprature_min(critical_temprature_min);
        this.setDevice_ttl(device_ttl);
        this.setOpen_lights_time(start_date);
        this.setClose_lights_time(finish_date);

    }

    public String getOpen_lights_time() {
        return open_lights_time;
    }

    public String getClose_lights_time() {
        return close_lights_time;
    }

    public String getAction() {
        return action;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDevice() {
        return device;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setOpen_lights_time(String open_lights_time) {
        this.open_lights_time = open_lights_time;
    }

    public void setClose_lights_time(String close_lights_time) {
        this.close_lights_time = close_lights_time;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public String getPort() {
        return port;
    }

    public void setDevice_ttl(int device_ttl) {
        this.device_ttl = device_ttl;
    }

    public void setCritical_temprature_max(double critical_temprature_max) {
        this.critical_temprature_max = critical_temprature_max;
    }

    public void setCritical_temprature_min(double critical_temprature_min) {
        this.critical_temprature_min = critical_temprature_min;
    }

    public void setCritical_humidity_max(double critical_humidity_max) {
        this.critical_humidity_max = critical_humidity_max;
    }

    public void setCritical_humidity_min(double critical_humidity_min) {
        this.critical_humidity_min = critical_humidity_min;
    }

    public int getDevice_ttl() {
        return device_ttl;
    }

    public double getCritical_temprature_max() {
        return critical_temprature_max;
    }

    public double getCritical_temprature_min() {
        return critical_temprature_min;
    }

    public double getCritical_humidity_max() {
        return critical_humidity_max;
    }

    public double getCritical_humidity_min() {
        return critical_humidity_min;
    }

}
