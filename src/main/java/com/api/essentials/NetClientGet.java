/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.essentials;

import com.api.model.User_Tempalte;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NetClientGet {

    private static HttpURLConnection conn;
    private final static Gson json = new Gson();

    private static void constractRequest(URL url) throws ProtocolException, MalformedURLException, IOException {
        conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
    }

    public static String requestFromKaaServer(String username, String password, URL url) {
        String results = null;
        try {
            constractRequest(url);
            String encoded = Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));  //Java 8
            conn.setRequestProperty("Authorization", "Basic " + encoded);
            if (conn.getResponseCode() != 200) {
                return ("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            results = deserializeResults(conn.getInputStream());
            conn.disconnect();
            return results;
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        return null;
    }

    public static String requestFromKaaServer(String username, String password, URL url, User_Tempalte jsonoj) throws IOException {
        String json = "{\"username\":\"" + jsonoj.getUsername() + "\",\"mail\": \"" + jsonoj.getMail() + "\" , \"authority\": \"" + jsonoj.getAuthority() + "\"}";
        return post(username, password, url, json);
    }

    private final static String deserializeResults(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(
                (stream)));
        StringBuilder sb = new StringBuilder();
        try {

            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output).append("\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(NetClientGet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            br.close();
        }
        return sb.toString();

    }

    public static String post(String username, String password, URL url, String data) throws IOException {
        System.out.println(data);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        System.out.println(username);
        System.out.println(password);
        String encoded = Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));  //Java 8
        con.setRequestProperty("Authorization", "Basic " + encoded);
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);

        sendData(con, data);

        return read(con.getInputStream());
    }

    protected static void sendData(HttpURLConnection con, String data) throws IOException {
        DataOutputStream wr = null;
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();
        } catch (IOException exception) {
            throw exception;
        } finally {
            closeQuietly(wr);
        }
    }

    private static String read(InputStream is) throws IOException {
        BufferedReader in = null;
        String inputLine;
        StringBuilder body;
        try {
            in = new BufferedReader(new InputStreamReader(is));

            body = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                body.append(inputLine);
            }
            in.close();
            System.out.println("passed");
            return body.toString();
        } catch (IOException ioe) {
            throw ioe;
        } finally {
            closeQuietly(in);
        }
    }

    protected static void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException ex) {

        }
    }

}
